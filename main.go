package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"flag"
	"fmt"
	"log"
	"math/big"
	"net"
	"os"
	"sync"
	"time"

	"github.com/mailproto/smtpd"
)

type TestUser struct {
	username string
	password string
}

func (t *TestUser) IsUser(ident string) bool {
	return true
}

func (t *TestUser) Password() string {
	return t.password
}

var (
	tlsGen    sync.Once
	tlsConfig *tls.Config
)

func main() {
	var (
		server   *smtpd.Server
		authType string
	)

	flag.StringVar(&authType, "auth", "None", "SMTP Server authentication [None, Plain, CRAMMD5]")
	help := flag.Bool("-help", false, "help")

	flag.Parse()

	if *help {
		flag.Usage()
		os.Exit(0)
	}

	server = smtpd.NewServer(func(msg *smtpd.Message) error {
		fmt.Println("Got message from:", msg.From)
		fmt.Println("Got message to:", msg.To)
		fmt.Println("Got message with subject:", msg.Subject)
		fmt.Println(string(msg.RawBody))
		fmt.Println("################################################")
		return nil
	})

	if authType != "None" {
		serverAuth := smtpd.NewAuth()

		switch authType {
		case "Plain":
			serverAuth.Extend("PLAIN", &smtpd.AuthPlain{
				Auth: func(username, password string) (smtpd.AuthUser, bool) {
					return &TestUser{}, true
				},
			})

		case "CRAMMD5":
			serverAuth.Extend("CRAM-MD5", &smtpd.AuthCramMd5{
				FindUser: func(username string) (smtpd.AuthUser, error) {
					return &TestUser{"user", "password"}, nil
				},
			})
		default:
			log.Println("no valid authentication type")
			flag.Usage()
			os.Exit(-1)
		}

		server.Auth = serverAuth
		server.TLSConfig = TestingTLSConfig()
	}

	go server.ListenAndServe("0.0.0.0:2500")
	<-server.Ready
	for {
		time.Sleep(10 * time.Second)
	}
}

func TestingTLSConfig() *tls.Config {
	tlsGen.Do(func() {

		priv, err := rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			panic(err)
		}
		serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
		serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
		if err != nil {
			panic(err)
		}
		xc := x509.Certificate{
			SerialNumber: serialNumber,
			Subject: pkix.Name{
				Organization: []string{"seide GmbH"},
			},
			IsCA:                  true,
			KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
			BasicConstraintsValid: true,
			IPAddresses:           []net.IP{net.ParseIP("0.0.0.0")},
		}

		b, err := x509.CreateCertificate(rand.Reader, &xc, &xc, &priv.PublicKey, priv)
		if err != nil {
			panic(err)
		}

		tlsConfig = &tls.Config{
			Certificates: []tls.Certificate{
				tls.Certificate{
					Certificate: [][]byte{b},
					PrivateKey:  priv,
					Leaf:        &xc,
				},
			},
			ClientAuth: tls.VerifyClientCertIfGiven,
			Rand:       rand.Reader,
		}
	})

	return tlsConfig
}
